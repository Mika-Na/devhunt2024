import { createApp } from 'vue'
//import './style.css'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import { createRouter, createWebHistory } from 'vue-router';
import Routes from './Routes'

const router = createRouter({
    history: createWebHistory(),
    routes: Routes
});

createApp(App).use(router).mount('#app')
