import { ref } from "vue"
import { Conversation, Message, User } from './typeData'
export const users = ref<User[]>([
  { idUser: 'aaa', name: 'mika', status: true },
  { idUser: 'bbb', name: 'Sims', status: false },
  { idUser: 'ccc', name: 'rabe', status: true },
  { idUser: 'ddd', name: 'Herilanto', status: true },
  { idUser: 'eee', name: 'Sylvio', status: false },
  { idUser: 'fff', name: 'Jessica', status: true },
  { idUser: 'ggg', name: 'ravao', status: true },
])

export const message = ref<Message[]>([
  { idMessage: 'aabbm', idUser: 'aaa', content: 'Tu est encore la ?' , idConversation : 'ttttt', date : '02-02-2024 10:20'},
  { idMessage: 'aaccm', idUser: 'ccc', content: 'Bonjour mika tieko' , idConversation : 'ttttt', date : '02-02-2024 10:20'},
  { idMessage: 'aaeem', idUser: 'eee', content: 'Bonjour Rakoto', idConversation : 'ttttt', date : '02-02-2024 10:20'},
  { idMessage: 'aaddm', idUser: 'ddd', content: 'Randria',idConversation : 'ttttt', date : '02-02-2024 10:20' },
  { idMessage: 'aapbm', idUser: 'bbb', content: 'Bonne nuit' ,idConversation : 'ttttt', date : '02-02-2024 10:20'},
  { idMessage: 'aaobm', idUser: 'bbb', content: 'Bonne nuit' ,idConversation : 'eeee', date : '02-02-2024 10:20'},
])
export const conversation = ref<Conversation[]>([
  {idConversation : 'ttttt', name : 'MJ 2SN'},
  {idConversation : 'eeeee', name : 'MJ NS2'},
  {idConversation : 'lllll', name : '2SN MJ'},
])