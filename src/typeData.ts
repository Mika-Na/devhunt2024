export type User = {
  idUser: string,
  name: string,
  status: boolean,
}
export type Message = {
  idMessage: string,
  idUser: string,
  idConversation: string,
  content: string,
  date: string,
}
export type Conversation = {
  idConversation: string,
  name: string,
}